package com.hillel.demo.core.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountDto {
    private Long id;
    private String ibanAccount;
    private BankDto bank;
}
