package com.hillel.demo.core.application.dto;

import com.hillel.demo.core.database.entity.Gender;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserDto {
    private Long id;
    private Gender gender;
    private String firstName;
    private String lastName;
    private Integer age;
    private HospitalDto hospital;
    private List<AccountDto> accounts;
    private String email;
}
