package com.hillel.demo.core.application.dto;

import com.hillel.demo.core.database.entity.Gender;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreateDto {
    private Gender gender;
    private String firstName;
    private String lastName;
    private Integer age;
    private Long hospitalId;
    private String email;
}
