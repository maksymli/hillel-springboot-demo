package com.hillel.demo.core.application.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsersGroupedByAgeDto {
    private Integer age;
    private Integer count;
}
