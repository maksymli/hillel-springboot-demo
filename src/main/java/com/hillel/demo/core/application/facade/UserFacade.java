package com.hillel.demo.core.application.facade;

import com.hillel.demo.core.application.dto.UserCreateDto;
import com.hillel.demo.core.application.dto.UserDto;
import com.hillel.demo.core.application.dto.UserUpdateDto;
import com.hillel.demo.core.application.dto.UsersGroupedByAgeDto;
import com.hillel.demo.core.domain.filter.UserFilter;
import com.hillel.demo.core.domain.model.Hospital;
import com.hillel.demo.core.domain.model.User;
import com.hillel.demo.core.domain.service.HospitalService;
import com.hillel.demo.core.domain.service.UserService;
import com.hillel.demo.core.mapper.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import util.Filters;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class UserFacade {
    private final UserService userService;
    private final HospitalService hospitalService;
    private final UserMapper userMapper;

    public List<UserDto> getAllUsers(Map<String, String> filters) {
        UserFilter.UserFilterBuilder filterBuilder = UserFilter.builder();

        filters.forEach((key, value) -> {
            switch (key) {
                case Filters.AGE:
                    filterBuilder.age(Integer.valueOf(value));
                    break;
                case Filters.FIRST_NAME:
                    filterBuilder.firstName(value);
                    break;
                case Filters.LAST_NAME:
                    filterBuilder.lastName(value);
                    break;
                case Filters.PAGE_NUMBER:
                    filterBuilder.pageNumber(value);
                    break;
                case Filters.PAGE_SIZE:
                    filterBuilder.pageSize(value);
                    break;
            }
        });

        UserFilter filter = filterBuilder.build();
        List<User> listUsers = userService.getByFilter(filter);
        return listUsers.stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public UserDto getUser(Long id) {
        User user = userService.getUser(id);
        return userMapper.toDto(user);
    }

    public UserDto createUser(UserCreateDto user) {
        User userModel = userMapper.toModel(user);
        Hospital hospital = hospitalService.getHospital(user.getHospitalId());
        userModel.setHospital(hospital);
        return userMapper.toDto(userService.createUser(userModel));
    }

    public UserDto updateUser(Long id, UserUpdateDto user) {
        User userModel = userMapper.toModel(user);
        Hospital hospital = hospitalService.getHospital(user.getHospitalId());
        userModel.setHospital(hospital);
        return userMapper.toDto(userService.updateUser(id, userModel));
    }

    public void deleteUser(Long id) {
        userService.deleteUser(id);
    }

    public List<UsersGroupedByAgeDto> geUsersGroupedByAge() {
        return userService.getUsersGroupedByAge().stream()
                .map(userMapper::toGroupedByAgeDto)
                .collect(Collectors.toList());
    }
}
