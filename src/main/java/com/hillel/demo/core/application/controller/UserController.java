package com.hillel.demo.core.application.controller;

import com.hillel.demo.core.application.dto.UserCreateDto;
import com.hillel.demo.core.application.dto.UserDto;
import com.hillel.demo.core.application.dto.UserUpdateDto;
import com.hillel.demo.core.application.dto.UsersGroupedByAgeDto;
import com.hillel.demo.core.application.facade.UserFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    private final UserFacade userFacade;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUser(@RequestParam(required = false) Map<String, String> filters) {
        return userFacade.getAllUsers(filters);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUserById(@PathVariable Long id) {
        return userFacade.getUser(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto createUser(@RequestBody UserCreateDto user) {
        return userFacade.createUser(user);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto updateUser(@RequestBody UserUpdateDto user, @PathVariable Long id) {
        return userFacade.updateUser(id, user);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        userFacade.deleteUser(id);
    }

    @GetMapping("/groupByAge")
    @ResponseStatus(HttpStatus.OK)
    public List<UsersGroupedByAgeDto> getUserGroupedByAge() {
        return userFacade.geUsersGroupedByAge();
    }
}
