package com.hillel.demo.core.domain.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class UserFilter {
    private final Integer age;
    private final String firstName;
    private final String lastName;
    private final String pageNumber;
    private final String pageSize;
}
