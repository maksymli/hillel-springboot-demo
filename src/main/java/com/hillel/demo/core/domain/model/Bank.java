package com.hillel.demo.core.domain.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Bank {
    private Long id;
    private String name;
    private String mfo;
}
