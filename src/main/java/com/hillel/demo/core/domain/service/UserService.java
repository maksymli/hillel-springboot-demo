package com.hillel.demo.core.domain.service;

import com.hillel.demo.core.database.entity.UserEntity;
import com.hillel.demo.core.database.repository.HospitalRepository;
import com.hillel.demo.core.database.repository.UserRepository;
import com.hillel.demo.core.domain.filter.UserFilter;
import com.hillel.demo.core.domain.model.Hospital;
import com.hillel.demo.core.domain.model.IUsersGroupedByAge;
import com.hillel.demo.core.domain.model.User;
import com.hillel.demo.core.mapper.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final HospitalRepository hospitalRepository;
    private final UserMapper userMapper;

    public User getUser(Long id) {
        return userMapper.toModel(userRepository.getOne(id));
    }

    public User createUser(@NonNull User user) {
        UserEntity userEntity = userMapper.toEntity(user);
        Long hospitalId = Optional.ofNullable(user.getHospital()).map(Hospital::getId).orElse(null);
        userEntity.setHospital(Objects.isNull(hospitalId) ? null : hospitalRepository.getOne(hospitalId));
        userRepository.save(userEntity);
        return userMapper.toModel(userEntity);
    }

    public User updateUser(Long id, User userModel) {
        UserEntity userEntity = userRepository.getOne(id);
        userEntity.setAge(userModel.getAge());
        userEntity.setFirstName(userModel.getFirstName());
        userEntity.setLastName(userModel.getLastName());
        userEntity.setGender(userModel.getGender());
        userRepository.save(userEntity);
        return userMapper.toModel(userEntity);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    public List<User> getByFilter(UserFilter filter) {
        return filter.getPageNumber() == null
                ? userRepository.findAllByFilter(filter.getAge(), filter.getFirstName(), filter.getLastName())
                    .stream()
                    .map(userMapper::toModel)
                    .collect(Collectors.toList())
                : userRepository.findAll(PageRequest.of(Integer.parseInt(filter.getPageNumber()), Integer.parseInt(filter.getPageSize())))
                    .stream()
                    .map(userMapper::toModel)
                    .collect(Collectors.toList());
    }

    public List<IUsersGroupedByAge> getUsersGroupedByAge() {
        return userRepository.groupAllByAge();
    }
}
