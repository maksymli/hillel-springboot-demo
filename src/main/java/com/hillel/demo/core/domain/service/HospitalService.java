package com.hillel.demo.core.domain.service;

import com.hillel.demo.core.database.repository.HospitalRepository;
import com.hillel.demo.core.domain.model.Hospital;
import com.hillel.demo.core.mapper.HospitalMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;
    private final HospitalMapper hospitalMapper;

    public Hospital getHospital(Long id) {
        return hospitalMapper.toModel(hospitalRepository.getOne(id));
    }
}
