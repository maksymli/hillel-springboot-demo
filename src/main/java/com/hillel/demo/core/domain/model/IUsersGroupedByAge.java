package com.hillel.demo.core.domain.model;

public interface IUsersGroupedByAge {
    Integer getAge();
    Integer getCount();
}
