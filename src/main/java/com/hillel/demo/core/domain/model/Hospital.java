package com.hillel.demo.core.domain.model;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Hospital {
    private Long id;
    private String name;
}
