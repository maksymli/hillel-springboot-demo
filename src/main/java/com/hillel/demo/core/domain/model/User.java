package com.hillel.demo.core.domain.model;

import com.hillel.demo.core.database.entity.Gender;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class User {
    private Long id;
    private Gender gender;
    private String firstName;
    private String lastName;
    private Integer age;
    private String email;
    private Hospital hospital;
    private List<Account> accounts;
}
