package com.hillel.demo.core.domain.model;

import com.hillel.demo.core.application.dto.BankDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account {
    private Long id;
    private String ibanAccount;
    private BankDto bank;
}
