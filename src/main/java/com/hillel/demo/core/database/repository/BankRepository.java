package com.hillel.demo.core.database.repository;

import com.hillel.demo.core.database.entity.BankEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BankRepository extends PagingAndSortingRepository<BankEntity, Long> {
    BankEntity findByName(String name);
}
