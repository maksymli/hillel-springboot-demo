package com.hillel.demo.core.database.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "users_id")
    private Long id;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column
    private Integer age;

    @Column(unique=true)
    private String email;

    @OneToOne
    private HospitalEntity hospital;

    @OneToMany(mappedBy = "user")
    private List<AccountEntity> accounts;

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
