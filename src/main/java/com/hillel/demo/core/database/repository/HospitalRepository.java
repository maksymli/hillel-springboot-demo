package com.hillel.demo.core.database.repository;

import com.hillel.demo.core.database.entity.HospitalEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalRepository extends JpaRepository<HospitalEntity, Long> {

}
