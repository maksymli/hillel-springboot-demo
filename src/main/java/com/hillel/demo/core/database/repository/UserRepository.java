package com.hillel.demo.core.database.repository;

import com.hillel.demo.core.database.entity.UserEntity;
import com.hillel.demo.core.domain.model.IUsersGroupedByAge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

    @Query("select u from UserEntity u where " +
            "(:age is null or u.age = :age) and" +
            "(:firstName is null or u.firstName = :firstName) and" +
            "(:lastName is null or u.lastName = :lastName)")
    List<UserEntity> findAllByFilter(@Param("age") Integer age,
                                     @Param("firstName") String firstName,
                                     @Param("lastName") String lastName);

    @Query(nativeQuery = true, value="select u.age, count(u) AS count from users u group by u.age")
    List<IUsersGroupedByAge> groupAllByAge();
}


