package com.hillel.demo.core.database.repository;

import com.hillel.demo.core.database.entity.AccountEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccountRepository extends PagingAndSortingRepository<AccountEntity, Long> {}
