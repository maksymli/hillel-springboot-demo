package com.hillel.demo.core.mapper;

import com.hillel.demo.core.application.dto.AccountDto;
import com.hillel.demo.core.database.entity.AccountEntity;
import com.hillel.demo.core.domain.model.Account;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class AccountMapper {

    private final ModelMapper mapper;

    public AccountDto toDto(Account account) {
        return Objects.isNull(account) ? null : mapper.map(account, AccountDto.class);
    }

    public Account toModel(AccountEntity account) {
        return Objects.isNull(account) ? null : mapper.map(account, Account.class);
    }
}
