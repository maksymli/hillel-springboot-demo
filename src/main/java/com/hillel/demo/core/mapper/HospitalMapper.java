package com.hillel.demo.core.mapper;

import com.hillel.demo.core.application.dto.HospitalDto;
import com.hillel.demo.core.database.entity.HospitalEntity;
import com.hillel.demo.core.domain.model.Hospital;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class HospitalMapper {

    private final ModelMapper mapper;

    public HospitalDto toDto(Hospital hospital) {
        return Objects.isNull(hospital) ? null : mapper.map(hospital, HospitalDto.class);
    }

    public Hospital toModel(HospitalEntity hospital) {
        return Objects.isNull(hospital) ? null : mapper.map(hospital, Hospital.class);
    }

    public HospitalEntity toEntity(Hospital hospital) {
        return Objects.isNull(hospital) ? null : mapper.map(hospital, HospitalEntity.class);
    }
}
