package com.hillel.demo.core.mapper;

import com.hillel.demo.core.application.dto.BankDto;
import com.hillel.demo.core.database.entity.BankEntity;
import com.hillel.demo.core.domain.model.Bank;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
public class BankMapper {

    private final ModelMapper mapper;

    public BankDto toDto (Bank bank) {
        return Objects.isNull(bank) ? null : mapper.map(bank, BankDto.class);
    }

    public Bank toModel(BankEntity bank) {
        return Objects.isNull(bank) ? null : mapper.map(bank, Bank.class);
    }
}
