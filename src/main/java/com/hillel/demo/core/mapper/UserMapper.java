package com.hillel.demo.core.mapper;

import com.hillel.demo.core.application.dto.UserCreateDto;
import com.hillel.demo.core.application.dto.UserDto;
import com.hillel.demo.core.application.dto.UserUpdateDto;
import com.hillel.demo.core.application.dto.UsersGroupedByAgeDto;
import com.hillel.demo.core.database.entity.UserEntity;
import com.hillel.demo.core.domain.model.IUsersGroupedByAge;
import com.hillel.demo.core.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public class UserMapper {
    private final ModelMapper mapper;

    public UserDto toDto(User user) {
        return Objects.isNull(user) ? null : mapper.map(user, UserDto.class);
    }

    public User toModel(UserEntity userEntity) {
        return Objects.isNull(userEntity) ? null : mapper.map(userEntity, User.class);
    }

    public User toModel(UserCreateDto user) {
        return Objects.isNull(user) ? null : mapper.map(user, User.class);
    }

    public User toModel(UserUpdateDto user) {
        return Objects.isNull(user) ? null : mapper.map(user, User.class);
    }

    public UserEntity toEntity(User user) {
        return Objects.isNull(user) ? null : mapper.map(user, UserEntity.class);
    }

    public UsersGroupedByAgeDto toGroupedByAgeDto(IUsersGroupedByAge IUsersGroupedByAge) {
        return Objects.isNull(IUsersGroupedByAge) ? null : mapper.map(IUsersGroupedByAge, UsersGroupedByAgeDto.class);
    }
}
